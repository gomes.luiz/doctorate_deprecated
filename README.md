# My Doctorate Project
A crawler for issue extraction information from many issue tracking systems.

## References 

### Machine Learning
1. [Naive Bayes Classifier](https://uc-r.github.io/naive_bayes)
2. [Caret Package - A Pratical Guide to Machine Learning](https://www.machinelearningplus.com/machine-learning/caret-package/)
3. [Visualizing Machine Learning Thresholds to Make Better Business Decisions](https://blog.insightdatascience.com/visualizing-machine-learning-thresholds-to-make-better-business-decisions-4ab07f823415)
4. [Measuring Performance in Caret Package](https://topepo.github.io/caret/measuring-performance.html#class) 
5. [Using Your Own Model in train](https://topepo.github.io/caret/using-your-own-model-in-train.html#Illustration4)

### Pandas
1. [Reshaping Pandas Dataframe](https://pandas.pydata.org/pandas-docs/stable/user_guide/reshaping.html)

### R Programming 
1. [UC Business Analytics R Programming Guinde](https://uc-r.github.io/)
2. [This R Data Import Tutorial Is Everything You Need](https://www.datacamp.com/community/tutorials/r-data-import-tutorial)

### Statistics 
1. [How to Calculate Nonparametric Statistical Hypothesis Tests in Python](https://machinelearningmastery.com/nonparametric-statistical-significance-tests-in-python/  )

### Text Mining & Natural Language Processing
1. [Creating text features with bag-of-words, n-grams, parts-of-speach and more](http://uc-r.github.io/creating-text-features)
2. [Top Books on Natural Language Processing](https://machinelearningmastery.com/books-on-natural-language-processing/)
3. [Generating WordClouds in Python](https://www.datacamp.com/community/tutorials/wordcloud-python)
4. [Text Analytics with Python: A Practical Real-World Approach to Gaining Actionable Insights from your Data](https://www.amazon.com/Text-Analytics-Python-Real-World-Actionable/dp/148422387X/ref=as_li_ss_tl?ie=UTF8&qid=1506457805&sr=8-1&keywords=text+analytics+with+python&linkCode=sl1&tag=edbholdings02-20&linkId=2b025608bbaa4e96327aa46b834c3900c5)
5. [Text Mining in R](https://www.oreilly.com/library/view/text-mining-with/9781491981641/ch04.html) 

## Visualização de Dados
1. [Become a Data Visualization Whiz with this Comprehensive Guide to Seaborn in Python](https://www.analyticsvidhya.com/blog/2019/09/comprehensive-data-visualization-guide-seaborn-python/)
2. [Textual Data Visualization](https://quanteda.io/articles/pkgdown/examples/plotting.html)
2. [Creating a Word Cloud in R](https://rpubs.com/brandonkopp/creating-word-clouds-in-r)

## Vídeos 

### Caret R Package
1. [Caret Package Webnar With Max Kuhn](https://www.youtube.com/watch?v=7Jbb2ItbTC4&list=WL&index=25)
